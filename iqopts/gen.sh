#!/bin/bash
protoc -I . -I $GOPATH/src/ -I $GOPATH/src -I /usr/local/include/ \
  --go_out=. \
  iqopts.proto

protoc -I .  -I $GOPATH/src/ -I $GOPATH/src -I /usr/local/include/ \
  --gotag_out=xxx="graphql+\"-\"":. \
  iqopts.proto

mv bitbucket.org/iqhive/iqhive-proto/iqopts/*go .
rm -Rf bitbucket.org
