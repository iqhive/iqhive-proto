module bitbucket.org/iqhive/iqhive-proto

go 1.15

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/srikrsna/protoc-gen-gotag v0.6.2
	google.golang.org/protobuf v1.28.0
)
